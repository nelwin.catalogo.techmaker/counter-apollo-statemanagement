import React from 'react';
import { useQuery } from "@apollo/client";
import { countVar, GET_COUNT, useCount } from "./cache/count";

function App() {
  const { data } = useQuery(GET_COUNT);
  const count = data.count;

  return (
    <div>
      <h1>{ count }</h1>
      <div>
        <AddCount toAdd={2} name="Add 2" />
        <AddCount toAdd={4} name="Add 4"/>
      </div>
      <div style={{ marginTop:"1rem" }}>
        <SubCount toSub={5} name="Sub 5" />
        <SubCount toSub={10} name="Sub 10" />
      </div>
    </div>
  );
}

function AddCount (props) {
  const { operations } = useCount(countVar);

  return (
    <button onClick={ () => operations.addCount(props.toAdd) }>{props.name}</button>
  );
}

function SubCount (props) {
  const { operations } = useCount(countVar);

  return (
    <button onClick={ () => operations.subCount(props.toSub) }>{props.name}</button>
  );
}

export default App;
