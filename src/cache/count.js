import { makeVar, gql } from "@apollo/client";

export const GET_COUNT = gql`
  query GetCount {
    count @client
  }
`;

// STATE
export const countVar = makeVar(0);

// MUTATION
// @count_var: Reactive State 
export const useCount = (count_var) => {
  const addCount = (count_to_add) => {
    const counts = count_var();
    count_var(counts + count_to_add);
  }

  const subCount = (count_to_sub) => {
    const counts = count_var();
    count_var(counts - count_to_sub);
  }

  return {
    operations: {
      addCount,
      subCount,
    }
  }
}
