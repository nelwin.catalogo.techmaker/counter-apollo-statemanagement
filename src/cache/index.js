import { InMemoryCache } from "@apollo/client";
import { countVar } from "./count";

export const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        count: {
          read () {
            return countVar();
          }
        }
      }
    }
  }
});
