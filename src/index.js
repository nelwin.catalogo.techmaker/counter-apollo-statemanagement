import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloClient, ApolloProvider } from "@apollo/client";
import { cache } from "./cache";
import App from './App';

const client = new ApolloClient({
  cache,
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
